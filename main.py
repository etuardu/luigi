#!/usr/bin/env python3
import pygame
import sys
import enum
import figura

pygame.init()
clock = pygame.time.Clock()
screen = pygame.display.set_mode((640, 480))

class States(enum.Enum):
  """Possibili valori dello stato corrente del gioco"""
  PLAY = enum.auto() # gioco in esecuzione
  WIN =  enum.auto() # gioco terminato, vittoria
  LOSE = enum.auto() # gioco terminato, fallimento

stella = figura.Figura("stella.png", (100, 300))
lumaca = figura.Figura("lumaca.png", (300, 300))

state = States.PLAY

print(list(figura.parabola(10, 1)))

while True:

  for event in pygame.event.get():
    if event.type == pygame.QUIT:
      sys.exit()

  pressed_keys = pygame.key.get_pressed()

  if state == States.PLAY:
    # il gioco è in normale esecuzione. Rilevo collisioni e
    # superamento ostacolo, muovo la stella in base alla
    # pressione dei tasti sinistra/destra/sù.

    if stella.colliding_with(lumaca):
      state = States.LOSE
      continue

    if stella.rect.x > 500: # ostacolo abbondantemente superato
      state = States.WIN
      continue

    if pressed_keys[pygame.K_LEFT]:
      stella.move_x(-20)
    if pressed_keys[pygame.K_RIGHT]:
      stella.move_x(20)
    if pressed_keys[pygame.K_UP]:
      stella.do_jump(15, 5)
      
    screen.fill((0, 0, 0)) # schermo nero

  if state == States.WIN:
    # il gioco è terminato con esito positivo.
    # Metto lo sfondo verde e smetto di leggere input
    # dalla tastiera.

    screen.fill((0, 100, 0)) # schermo verde

  if state == States.LOSE:
    # il gioco è terminato con esito negativo.
    # Metto lo sfondo rosso e do la possibilità di
    # giocare di nuovo alla pressione del tasto spazio.

    screen.fill((100, 0, 0)) # schermo rosso

    if pressed_keys[pygame.K_SPACE]:
      # riavvia il gioco
      stella.reset_position()
      state = States.PLAY

  lumaca.blit_next(screen)
  stella.blit_next(screen)

  pygame.display.update()
  clock.tick(30)

