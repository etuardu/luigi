#!/usr/bin/env python3
import pygame

class Figura:
  """Un oggetto che include il necessario
  per un'immagine sullo schermo (surface, rect, mask)"""

  def __init__(self, img, position):
    """
    img: percorso dell'immagine png con trasparenze
    position: coordinate iniziali sullo schermo nella forma (x, y)
    """
    self.surface = pygame.image.load(img).convert_alpha()
    self.rect = self.surface.get_rect()
    self.rect.x, self.rect.y = self.original_x, self.original_y = position
    self.mask = pygame.mask.from_surface(self.surface)
    self.jump = []

  def colliding_with(self, other):
    """Restituisce True in caso di collisione, False altrimenti
    other: altro oggetto di tipo Figura con cui controllare la collisione
    """
    return self.rect.colliderect(other.rect) \
      and self.mask.overlap(other.mask, (
        other.rect.x - self.rect.x,
        other.rect.y - self.rect.y,
      ))

  def do_jump(self, duration, power=2):
    """Imposta il salto.
    duration - durata del salto in frame
    power - esplosività del salto
    L'altezza del salto è determinata sia da
    duration che da power.
    """

    if self.jump:
      # sta già saltando
      return 

    half_duration = int(duration / 2)
    # metà della durata intesa come quantità di frame,
    # senza virgola.

    vals = [ i**2 for i in range(0-half_duration, half_duration+1) ]
    # lista di lunghezza "duration" con valori che iniziano e
    # finiscono sullo stesso valore, decrementando sempre meno
    # rapidamente (con andamento quadratico) fino allo zero e
    # poi incrementando di nuovo in modo simmetrico.
    #
    # Es. se duration = 7
    #
    #        │
    #       9┤ X                 X
    #        │                    
    #        │                    
    #        │                    
    #        │                    
    #       4┤    X           X   
    #        │                    
    #        │                    
    #       1┤       X     X      
    #       0└─┬──┬──┬──X──┬──┬──┬──
    #          0  1  2  3  4  5  6
    #
    # vals = [ 9, 4, 1, 0, 1, 4, 9 ]

    vals = [ v - vals[0] for v in vals ]
    # scalo tutti i valori sottraendogli
    # il valore massimo (che si trova in posizione 0).
    # In questo modo i valori partono con 0,
    # scendono in negativo e poi tornano a
    # 0, così sommandoli alla coordinata y iniziale
    # del personaggio si ottiene un salto che inizia
    # e finisce su quest'ultima.
    #
    #          0  1  2  3  4  5  6
    #       0┌─X──┴──┴──┴──┴──┴──X──
    #        │                    
    #        │                    
    #        │                    
    #        │                    
    #      -5┤    X           X   
    #        │                    
    #        │                    
    #      -8┤       X     X      
    #      -9┤          X
    #        │                    
    #
    # vals = [ 0,-5,-8,-9,-8,-5, 0 ]

    vals = [ int(v * power) for v in vals ]
    # moltiplico tutti i valori per power.
    # In questo modo è possibile ottenere un
    # salto più esplosivo (che raggiunge altezze
    # maggiori a parità di tempo) quando
    # power > 2 (default) oppure meno 
    # quando 0 < power < 2.
    # I valori iniziali e finali restano
    # correttamente invariati a 0.

    self.jump = vals

  def blit_next(self, screen):

    if self.jump:
      # avanza nel salto
      self.rect.y = self.original_y + self.jump.pop(0)

    screen.blit(self.surface, self.rect)

  def move_x(self, amount):
    """Sposta la figura sull'asse x.
    Limita lo spostamento alle dimensioni della finestra,
    ossia impedisce alla figura di uscire fuori dall'area visible
    dello schermo sia a destra che a sinistra.
    """
    x = self.rect.x + amount
    x = max(x, 0) # limite sinistro
    x = min(
      x,
      pygame.display.get_surface().get_width() - self.rect.w
    ) # limite destro
    self.rect.x = x

  def reset_position(self):
    """Riposizione la figura sulle coordinate iniziali"""
    self.rect.x = self.original_x
    self.rect.y = self.original_y
    self.jump = [] # annulla eventuale salto
